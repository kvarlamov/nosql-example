﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class PreferenceService
    {
        private readonly HttpClient _client;

        public PreferenceService(HttpClient client)
        {
            _client = client;
        }

        public async Task<List<Preference>> GetPreferences()
        {
            var response = await _client.GetAsync("redisapi/v1/preferences");

            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<Preference>>(responseString);
        }

        public async Task<Preference> GetPreference(Guid id)
        {
            var response = await _client.GetAsync($"redisapi/v1/preferences/{id}");

            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Preference>(responseString);
        }
    }
}