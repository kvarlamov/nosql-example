﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RedisService.Models;
using RedisService.Repository;
using StackExchange.Redis;

namespace RedisService.Controllers
{
    [ApiController]
    [Route("redisapi/v1/[controller]")]
    public class PreferencesController : ControllerBase
    {
        private readonly IPreferenceCache _cache;

        public PreferencesController(IPreferenceCache cache)
        {
            _cache = cache;
        }
        
        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<Preference>>> GetPreferencesAsync()
        {
            var preferences = await _cache.GetAllAsync();

            return Ok(preferences);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Preference>> GetPreferenceAsync(string id)
        {
            var preference = await _cache.GetByIdAsync(id);
            return Ok(preference);
        }
    }
}