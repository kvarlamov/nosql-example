﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RedisService.Models;
using StackExchange.Redis;

namespace RedisService.Repository
{
    public interface IPreferenceCache
    {
        Task<List<Preference>> GetAllAsync();
        Task<Preference> GetByIdAsync(RedisKey id);
        Task<Preference> UpdateAsync(Preference item);
        Task DeleteAsync(RedisKey id);
    }
}