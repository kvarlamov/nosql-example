﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RedisService.Models;
using StackExchange.Redis;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace RedisService.Repository
{
    public class PreferenceCache : IPreferenceCache
    {
        private readonly IDatabase _db;
        private readonly IServer _server;

        public PreferenceCache(IConfiguration config)
        {
            var redis = ConnectionMultiplexer.Connect(config["CacheSettings:ConnectionString"]);
            _server = redis.GetServer(config["CacheSettings:ConnectionString"]);
            _db = redis.GetDatabase();
        }

        public async Task<List<Preference>> GetAllAsync()
        {
            var preferences = new List<Preference>();

            foreach (var key in _server.Keys())
            {
                var preference = await GetByIdAsync(key);
                preferences.Add(preference);
            }

            return preferences;
        }

        public async Task<Preference> GetByIdAsync(RedisKey id)
        {
            var preference = await _db.StringGetAsync(id);
            if (preference.HasValue)
                return JsonConvert.DeserializeObject<Preference>(preference);

            throw new Exception($"Preference with provided id{id} not found");
        }

        public async Task<Preference> UpdateAsync(Preference item)
        {
            await _db.SetAddAsync(item.Id.ToString(), JsonConvert.SerializeObject(item));
            return await GetByIdAsync(item.Id.ToString());
        }

        public async Task DeleteAsync(RedisKey id)
        {
            await _db.KeyDeleteAsync(id);
        }
    }
}