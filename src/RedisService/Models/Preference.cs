﻿using System;

namespace RedisService.Models
{
    public class Preference
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}