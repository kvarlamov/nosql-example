﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RedisService.Models;
using RedisService.Repository;
using StackExchange.Redis;

namespace RedisService.Data
{
    public static class RedisCacheHelper
    {
        public static void Seed(IConfiguration config)
        {
            var connectionString = config["CacheSettings:ConnectionString"];
            var redis = ConnectionMultiplexer.Connect(connectionString);
            var db = redis.GetDatabase();
            foreach (var preference in Preferences)
            {
                db.StringSetAsync(preference.Id.ToString(), JsonConvert.SerializeObject(preference));
            }
        }
        
        private static List<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Name = "Театр",
            },
            new Preference()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья",
            },
            new Preference()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Дети",
            }
        };
    }
}