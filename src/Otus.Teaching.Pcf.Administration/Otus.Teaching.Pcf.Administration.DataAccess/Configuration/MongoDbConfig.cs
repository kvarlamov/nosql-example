﻿using Microsoft.Extensions.Configuration;
using Otus.Teaching.Pcf.Administration.WebHost;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Configuration
{
    public class MongoDbConfig : IMongoDbConfig
    {
        public string Database { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public string User { get; set; }
        public string Password { get; set; }

        public string ConnectionString { get; }

        public MongoDbConfig(IConfiguration config)
        {
            if (config != null)
            {
                Database = config.GetValue<string>("MongoDB:Database");
                Host = config["MongoDB:Host"];
                Port = int.Parse(config["MongoDB:Port"]);
                User = config["MongoDB:User"];
                Password = config["MongoDB:Password"];
                ConnectionString = string.IsNullOrEmpty(User) || string.IsNullOrEmpty(Password)
                    ? $@"mongodb://{Host}:{Port}"
                    : $@"mongodb://{User}:{Password}@{Host}:{Port}";
            }
        }
    }
}