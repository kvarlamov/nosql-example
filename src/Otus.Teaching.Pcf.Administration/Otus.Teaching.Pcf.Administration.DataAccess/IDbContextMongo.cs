﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess
{
    public interface IDbContextMongo
    {
        IMongoCollection<Employee> Employees { get; }
        IMongoCollection<Role> Roles { get; }
    }
}