﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class RoleRepository : IRepository<Role>
    {
        private readonly IDbContextMongo _db;

        public RoleRepository(IDbContextMongo db)
        {
            _db = db;
        }
        public async Task<IEnumerable<Role>> GetAllAsync()
        {
            return await _db.Roles.Find(p => true).ToListAsync();
        }

        public async Task<Role> GetByIdAsync(Guid id)
        {
            return await _db.Roles.Find(p => p.Id == id).FirstOrDefaultAsync();
        }

        public Task<IEnumerable<Role>> GetRangeByIdsAsync(List<Guid> ids)
        {
            throw new NotImplementedException();
        }

        public Task<Role> GetFirstWhere(Expression<Func<Role, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Role>> GetWhere(Expression<Func<Role, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task AddAsync(Role entity)
        {
            await _db.Roles.InsertOneAsync(entity);
        }

        public async Task<bool> UpdateAsync(Role entity)
        {
            var updateResult = await _db.Roles.ReplaceOneAsync(filter: e => e.Id == entity.Id, replacement: entity);
            return updateResult.IsAcknowledged && updateResult.ModifiedCount > 0;
        }

        public async Task<bool> DeleteAsync(Role entity)
        {
            FilterDefinition<Role> filter = Builders<Role>.Filter.Eq(e => e.Id, entity.Id);
            DeleteResult deleteResult = await _db.Roles.DeleteOneAsync(filter);
            return deleteResult.IsAcknowledged && deleteResult.DeletedCount > 0;
        }
    }
}