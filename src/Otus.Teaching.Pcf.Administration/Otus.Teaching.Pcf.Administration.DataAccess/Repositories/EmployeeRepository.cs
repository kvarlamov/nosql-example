﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class EmployeeRepository : IRepository<Employee>
    {
        private readonly IDbContextMongo _db;

        public EmployeeRepository(IDbContextMongo db)
        {
            _db = db;
        }
        public async Task<IEnumerable<Employee>> GetAllAsync()
        {
            return await _db.Employees.Find(p => true).ToListAsync();
        }

        public async Task<Employee> GetByIdAsync(Guid id)
        {
            return await _db.Employees.Find(p => p.Id == id).FirstOrDefaultAsync();
        }

        public Task<IEnumerable<Employee>> GetRangeByIdsAsync(List<Guid> ids)
        {
            throw new NotImplementedException();
        }

        public Task<Employee> GetFirstWhere(Expression<Func<Employee, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Employee>> GetWhere(Expression<Func<Employee, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task AddAsync(Employee entity)
        {
            await _db.Employees.InsertOneAsync(entity);
        }

        public async Task<bool> UpdateAsync(Employee entity)
        {
            var updateResult = await _db.Employees.ReplaceOneAsync(filter: e => e.Id == entity.Id, replacement: entity);
            return updateResult.IsAcknowledged && updateResult.ModifiedCount > 0;
        }

        public async Task<bool> DeleteAsync(Employee entity)
        {
            FilterDefinition<Employee> filter = Builders<Employee>.Filter.Eq(e => e.Id, entity.Id);
            DeleteResult deleteResult = await _db.Employees.DeleteOneAsync(filter);
            return deleteResult.IsAcknowledged && deleteResult.DeletedCount > 0;
        }
    }
}