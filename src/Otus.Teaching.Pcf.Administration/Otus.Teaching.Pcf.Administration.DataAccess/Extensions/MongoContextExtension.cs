﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.DataAccess.Data;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Extensions
{
    public static class MongoContextExtension
    {
        public static void SeedData(this IDbContextMongo context)
        {
            if (!context.Employees.Find(p => true).Any())
            {
                context.Roles.InsertMany(FakeDataFactory.Roles);
                context.Employees.InsertMany(FakeDataFactory.Employees);
            }
        }
    }
}