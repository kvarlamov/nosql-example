﻿using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess.Data;
using Otus.Teaching.Pcf.Administration.WebHost;

namespace Otus.Teaching.Pcf.Administration.DataAccess
{
    public class DbContextMongo : IDbContextMongo
    {
        private readonly IMongoDatabase _db;
        public IMongoCollection<Employee> Employees => _db.GetCollection<Employee>("employees");
        public IMongoCollection<Role> Roles => _db.GetCollection<Role>("roles");

        public DbContextMongo(IMongoDbConfig config)
        {
            var client = new MongoClient(config.ConnectionString);
            _db = client.GetDatabase(config.Database);
        }
    }
}